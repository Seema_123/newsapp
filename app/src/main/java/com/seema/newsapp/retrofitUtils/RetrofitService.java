package com.seema.newsapp.retrofitUtils;

import com.seema.newsapp.module.NewsResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitService {
    @GET("v1/articles")
    Call<NewsResult> getNews(@Query("source") String source, @Query("apiKey") String apiKey);
}
