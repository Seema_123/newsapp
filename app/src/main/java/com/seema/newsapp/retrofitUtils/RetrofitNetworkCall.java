package com.seema.newsapp.retrofitUtils;

import android.content.Context;
import android.util.Log;

import com.seema.newsapp.interfaces.UpdateUi;
import com.seema.newsapp.module.NewsResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitNetworkCall {
    private static final String TAG = RetrofitNetworkCall.class.getName();
    private static RetrofitService retrofitService;
    UpdateUi updateUi;

    public static void getNews(final Context ctx, String id) {
        retrofitService = ApiUtils.getRetrofitServiceForNews();
        String source;
        if (!id.equals("")) {
            source = id;
        } else {
            source = "google-news";
        }
        String sortBy = "latest";
        String apiKey = "9159724ccde147d1a7d3d1381b562afa";
        final UpdateUi updateUi = (UpdateUi) ctx;
        retrofitService.getNews(source,/* sortBy,*/ apiKey).enqueue(new Callback<NewsResult>() {
            @Override
            public void onResponse(Call<NewsResult> call, Response<NewsResult> response) {


                if (response.isSuccessful()) {
                    updateUi.updateNews(response.body());
                } else {
                    Log.d(TAG, "onResponse: " + response.message());
                    updateUi.error(response.message());
                }
            }

            @Override
            public void onFailure(Call<NewsResult> call, Throwable t) {
                Log.d(TAG, "onResponse: " + t.getMessage());
                updateUi.error(t.getMessage());
            }
        });
    }

}
