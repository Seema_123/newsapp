package com.seema.newsapp.retrofitUtils;

public class ApiUtils {
    private static final String BASE_URL_NEWS = "https://newsapi.org/";

    public static RetrofitService getRetrofitServiceForNews(){
        return RetrofitClient.getClient(BASE_URL_NEWS).create(RetrofitService.class);
    }
}
