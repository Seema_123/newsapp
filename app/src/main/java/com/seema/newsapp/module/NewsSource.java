package com.seema.newsapp.module;

public class NewsSource {
    private String name, id, category;
    private boolean isFav;

    public NewsSource() {
    }

    public NewsSource(String name, String id, String category, boolean isFav) {
        this.name = name;
        this.id = id;
        this.category =  category;
        this.isFav = isFav;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isFav() {
        return isFav;
    }

    public void setFav(boolean fav) {
        isFav = fav;
    }
}
