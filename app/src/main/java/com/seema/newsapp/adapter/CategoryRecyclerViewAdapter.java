package com.seema.newsapp.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.seema.newsapp.R;
import com.seema.newsapp.activity.SplashScreenActivity;

import java.util.ArrayList;

public class CategoryRecyclerViewAdapter extends RecyclerView.Adapter<CategoryRecyclerViewAdapter.ViewHolder> {

    private Context ctx;
    private ArrayList<String> categoryArrayList;
    private View lastSelectedView;

    private CustomOnClickListener listener;
    private static int lastItemPosition = -1;

    public CategoryRecyclerViewAdapter(Context ctx, ArrayList<String> cat) {
        this.ctx = ctx;
        this.categoryArrayList = cat;
        listener = (CustomOnClickListener) ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(R.layout.cat_layout, parent, false);
        return new ViewHolder(view);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final String category = categoryArrayList.get(position);
        holder.name.setText(category);

        if (lastItemPosition == -1 && category.equals(SplashScreenActivity.GENERAL)) {
            holder.name.setBackground(ctx.getResources().getDrawable(R.drawable.select_background));
            lastSelectedView = holder.name;
            lastItemPosition = position;
        }

        if (lastItemPosition == position) {
            holder.name.setBackground(ctx.getResources().getDrawable(R.drawable.select_background));
        } else {
            holder.name.setBackground(ctx.getResources().getDrawable(R.drawable.border));
        }
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lastSelectedView != null) {
                    lastSelectedView.setBackground(ctx.getResources().getDrawable(R.drawable.border));
                }
                holder.name.setBackground(ctx.getResources().getDrawable(R.drawable.select_background));
                lastSelectedView = holder.name;
                lastItemPosition = position;

                listener.onClick(category);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.newsCatName);
        }
    }

    public interface CustomOnClickListener {
        void onClick(String category);
    }
}
