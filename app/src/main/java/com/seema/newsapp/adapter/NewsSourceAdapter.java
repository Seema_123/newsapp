package com.seema.newsapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.seema.newsapp.R;
import com.seema.newsapp.activity.NewsDetails;
import com.seema.newsapp.database.DBHelper;
import com.seema.newsapp.module.NewsSource;

import java.util.ArrayList;

public class NewsSourceAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NewsSource> newsSources;


    public NewsSourceAdapter(Context context, ArrayList<NewsSource> newsSources) {
        this.context = context;
        this.newsSources = newsSources;
    }

    @Override

    public int getCount() {
        return newsSources.size();
    }

    @Override
    public Object getItem(int i) {
        return newsSources.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Holder holder;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.source_layout, viewGroup, false);
            holder = new Holder();
            holder.newsSource = view.findViewById(R.id.newsSourceName);
            holder.fav = view.findViewById(R.id.favImage);
            holder.root = view.findViewById(R.id.root);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }

        final NewsSource source = newsSources.get(i);
        final String sourceName = source.getName();
        final String id = source.getId();
        holder.newsSource.setText(sourceName);

        if (source.isFav()) {
            holder.fav.setImageResource(R.drawable.ic_favorite_black_24dp);
        } else {
            holder.fav.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        }

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NewsDetails.class);
                intent.putExtra("source", id);
                intent.putExtra("name",sourceName);
                context.startActivity(intent);
            }
        });

        holder.fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (source.isFav()) {
                    holder.fav.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                    source.setFav(false);
                } else {
                    holder.fav.setImageResource(R.drawable.ic_favorite_black_24dp);
                    source.setFav(true);
                }
                DBHelper.init(context);
                DBHelper.insertNewsSource(source);
                notifyDataSetChanged();
            }
        });
        return view;
    }

    public class Holder {
        RelativeLayout root;
        TextView newsSource;
        ImageView fav;
    }

    public void notify(ArrayList<NewsSource> sources) {
        this.newsSources = sources;
        notifyDataSetChanged();
    }
}
