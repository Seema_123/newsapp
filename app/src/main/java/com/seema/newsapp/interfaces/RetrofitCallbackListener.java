package com.seema.newsapp.interfaces;

public interface RetrofitCallbackListener {
    void onCallback(Object result);
    void onError(String message);
}
