package com.seema.newsapp.interfaces;

import com.seema.newsapp.module.NewsResult;

public interface UpdateUi {
    void updateNews(NewsResult newsResult);
    void error(String error);
}
