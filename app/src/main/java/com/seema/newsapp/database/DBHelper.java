package com.seema.newsapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.seema.newsapp.module.NewsSource;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper instance = null;
    private static SQLiteDatabase database = null;

    private static String DBNAME = "news.db";
    private static int VERSION = 1;

    private static String TABLENAME = "News";
    private static String ID = "id";
    private static String NEWS_NAME = "newsName";
    private static String NEWS_SHORT_NAME = "newsShortName";
    private static String CAT = "category";
    private static String FAV = "favorite";


    public static void init(Context context) {
        if (instance == null) {
            synchronized (DBHelper.class) {
                if (instance == null) {
                    instance = new DBHelper(context);
                }
            }
        }
    }

    private static SQLiteDatabase getDataBase() {

        if (database == null) {
            if (instance != null) {
                database = instance.getWritableDatabase();
            }
        }
        return database;

    }

    public static void deactivate() {
        if (null != instance && database.isOpen()) {
            database.close();
        }
        instance = null;
        database = null;
    }


    private DBHelper(Context context) {
        super(context, DBNAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String query = "CREATE TABLE IF NOT EXISTS " + TABLENAME + " ( "
                + ID + " INTEGER primary key autoincrement, "
                + NEWS_NAME + " TEXT NOT NULL, "
                + NEWS_SHORT_NAME + " TEXT NOT NULL, "
                + CAT + " TEXT NOT NULL, "
                + FAV + " INTEGER NOT NUll) ";
        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLENAME);
        onCreate(sqLiteDatabase);
    }

    public static void insertNewsSource(NewsSource newsSource) {

        ContentValues cv = new ContentValues();
        cv.put(NEWS_NAME, newsSource.getName());
        cv.put(NEWS_SHORT_NAME, newsSource.getId());
        cv.put(CAT, newsSource.getCategory());
        cv.put(FAV, newsSource.isFav() ? 1 : 0);

        SQLiteDatabase db = getDataBase();
        Cursor cursor = db.query(TABLENAME, null, NEWS_SHORT_NAME + "=?", new String[]{newsSource.getId()}, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            db.update(TABLENAME, cv, NEWS_SHORT_NAME + "=?", new String[]{newsSource.getId()});
        } else {
            db.insert(TABLENAME, null, cv);
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    public static ArrayList<NewsSource> getNewsSources(String cat) {
        SQLiteDatabase db = getDataBase();
        ArrayList<NewsSource> newsSources = new ArrayList<>();
        Cursor cursor = db.query(TABLENAME, null, CAT + "=?", new String[]{cat}, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                String name = cursor.getString(cursor.getColumnIndex(NEWS_NAME));
                String name_short = cursor.getString(cursor.getColumnIndex(NEWS_SHORT_NAME));
                String cate = cursor.getString(cursor.getColumnIndex(CAT));
                int fav = cursor.getInt(cursor.getColumnIndex(FAV));
                NewsSource newsSource = new NewsSource(name, name_short, cate, fav == 1);
                newsSources.add(newsSource);
            } while (cursor.moveToNext());
        }

        return newsSources;
    }

    public static ArrayList<NewsSource> getNewsSourcesFav() {
        SQLiteDatabase db = getDataBase();
        ArrayList<NewsSource> newsSources = new ArrayList<>();
        Cursor cursor = db.query(TABLENAME, null, FAV + "=?", new String[]{"1"}, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                String name = cursor.getString(cursor.getColumnIndex(NEWS_NAME));
                String name_short = cursor.getString(cursor.getColumnIndex(NEWS_SHORT_NAME));
                String cate = cursor.getString(cursor.getColumnIndex(CAT));
                int fav = cursor.getInt(cursor.getColumnIndex(FAV));
                NewsSource newsSource = new NewsSource(name, name_short, cate, fav == 1);
                newsSources.add(newsSource);
            } while (cursor.moveToNext());
        }

        return newsSources;
    }


}
