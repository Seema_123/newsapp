package com.seema.newsapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.seema.newsapp.R;
import com.seema.newsapp.database.DBHelper;
import com.seema.newsapp.module.NewsSource;

import java.util.ArrayList;
import java.util.Map;

public class SplashScreenActivity extends AppCompatActivity {
    public static final String FAVORITE = "Favorite";
    public static final String GENERAL = "General";
    public static final String TECH = "Technology";
    public static final String ENT = "Entertainment";
    public static final String SPORTS = "Sports";
    public static final String SCI = "Science and Nature";
    public static final String MUSIC = "Music";
    public static final String GAME = "Gaming";
    public static final String BUS = "Business";
    public static final String POLITICAL = "Political";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ArrayList<NewsSource> newsSources = newsSourrceList();

        DBHelper.init(this);
        if (DBHelper.getNewsSourcesFav().size() == 0) {
            for (int i = 0; i < newsSources.size(); i++) {
                DBHelper.insertNewsSource(newsSources.get(i));
            }
        }
    }

    public ArrayList<NewsSource> newsSourrceList() {
//        Map<String,String> newsSource = new HashMap<>();
        ArrayList<NewsSource> newsSource = new ArrayList<>();

        newsSource.add(new NewsSource("Google News", getNewsid("Google News"), GENERAL, false));
        newsSource.add(new NewsSource("ABC News (AU)", getNewsid("ABC News AU"), GENERAL, false));
        newsSource.add(new NewsSource("Al Jazeera English", getNewsid("Al Jazeera English"), GENERAL, false));
        newsSource.add(new NewsSource("Ars Technica", getNewsid("Ars Technica"), TECH, false));
        newsSource.add(new NewsSource("Associated Press", getNewsid("Associated Press"), GENERAL, false));
        newsSource.add(new NewsSource("BBC News", getNewsid("BBC News"), GENERAL, false));
        newsSource.add(new NewsSource("BBC Sport", getNewsid("BBC Sport"), SPORTS, false));
        newsSource.add(new NewsSource("Bild", getNewsid("Bild"), GENERAL, false));
        newsSource.add(new NewsSource("Bloomberg", getNewsid("Bloomberg"), BUS, false));
        newsSource.add(new NewsSource("Breitbart News", getNewsid("Breitbart News"), POLITICAL, false));
        newsSource.add(new NewsSource("Business Insider", getNewsid("Business Insider"), BUS, false));
        newsSource.add(new NewsSource("Business Insider (UK)", getNewsid("Business Insider UK"), BUS, false));
        newsSource.add(new NewsSource("Buzzfeed", getNewsid("Buzzfeed"), ENT, false));
        newsSource.add(new NewsSource("CNBC", getNewsid("CNBC"), BUS, false));
        newsSource.add(new NewsSource("CNN", getNewsid("CNN"), GENERAL, false));
        newsSource.add(new NewsSource("Daily Mail", getNewsid("Daily Mail"), ENT, false));
        newsSource.add(new NewsSource("Der Tagesspiegel", getNewsid("Der Tagesspiegel"), GENERAL, false));
        newsSource.add(new NewsSource("Die Zeit", getNewsid("Die Zeit"), BUS, false));
        newsSource.add(new NewsSource("Engadget", getNewsid("Engadget"), TECH, false));
        newsSource.add(new NewsSource("Entertainment Weekly", getNewsid("Entertainment Weekly"), ENT, false));
        newsSource.add(new NewsSource("ESPN", getNewsid("ESPN"), SPORTS, false));
        newsSource.add(new NewsSource("ESPN Cric Info", getNewsid("ESPN Cric Info"), SPORTS, false));
        newsSource.add(new NewsSource("Financial Times", getNewsid("Financial Times"), BUS, false));
        newsSource.add(new NewsSource("Focus", getNewsid("Focus"), GENERAL, false));
        newsSource.add(new NewsSource("Football Italia", getNewsid("Football Italia"), SPORTS, false));
        newsSource.add(new NewsSource("Fortune", getNewsid("Fortune"), BUS, false));
        newsSource.add(new NewsSource("FourFourTwo", getNewsid("FourFourTwo"), SPORTS, false));
        newsSource.add(new NewsSource("Fox Sports", getNewsid("Fox Sports"), SPORTS, false));

        newsSource.add(new NewsSource("Gruenderszene", getNewsid("Gruenderszene"), TECH, false));
        newsSource.add(new NewsSource("Hacker News", getNewsid("Hacker News"), TECH, false));
        newsSource.add(new NewsSource("Handelsblatt", getNewsid("Handelsblatt"), BUS, false));
        newsSource.add(new NewsSource("IGN", getNewsid("IGN"), GAME, false));
        newsSource.add(new NewsSource("Independent", getNewsid("Independent"), GENERAL, false));
        newsSource.add(new NewsSource("Mashable", getNewsid("Mashable"), ENT, false));
        newsSource.add(new NewsSource("Metro", getNewsid("Metro"), GENERAL, false));
        newsSource.add(new NewsSource("Mirror", getNewsid("Mirror"), GENERAL, false));
        newsSource.add(new NewsSource("MTV News", getNewsid("MTV News"), MUSIC, false));
        newsSource.add(new NewsSource("MTV News (UK)", getNewsid("MTV News UK"), MUSIC, false));
        newsSource.add(new NewsSource("National Geographic", getNewsid("National Geographic "), SCI, false));
        newsSource.add(new NewsSource("New Scientist", getNewsid("New Scientist"), SCI, false));
        newsSource.add(new NewsSource("Newsweek", getNewsid("Newsweek"), GENERAL, false));
        newsSource.add(new NewsSource("New York Magazine", getNewsid("New York Magazine"), GENERAL, false));
        newsSource.add(new NewsSource("NFL News", getNewsid("NFL News"), SPORTS, false));
        newsSource.add(new NewsSource("Polygon", getNewsid("Polygon"), GAME, false));
        newsSource.add(new NewsSource("Recode", getNewsid("Recode"), TECH, false));
        newsSource.add(new NewsSource("Reddit /r/all", getNewsid("Reddit r all"), GENERAL, false));
        newsSource.add(new NewsSource("Reuters", getNewsid("Reuters"), GENERAL, false));
        newsSource.add(new NewsSource("Spiegel Online", getNewsid("Spiegel Online"), GENERAL, false));
        newsSource.add(new NewsSource("T3n", getNewsid("T3n"), TECH, false));
        newsSource.add(new NewsSource("TalkSport", getNewsid("TalkSport"), SPORTS, false));
        newsSource.add(new NewsSource("TechCrunch", getNewsid("TechCrunch"), TECH, false));
        newsSource.add(new NewsSource("TechRadar", getNewsid("TechRadar"), TECH, false));
        newsSource.add(new NewsSource("The Economist", getNewsid("The Economist"), BUS, false));
        newsSource.add(new NewsSource("The Guardian (AU)", getNewsid("The Guardian AU"), GENERAL, false));
        newsSource.add(new NewsSource("The Guardian (UK)", getNewsid("The Guardian UK"), GENERAL, false));
        newsSource.add(new NewsSource("The Hindu", getNewsid("The Hindu"), GENERAL, false));
        newsSource.add(new NewsSource("The Huffington Post", getNewsid("The Huffington Post"), GENERAL, false));
        newsSource.add(new NewsSource("The Lad Bible", getNewsid("The Lad Bible"), ENT, false));
        newsSource.add(new NewsSource("The New York Times", getNewsid("The New York Times"), GENERAL, false));
        newsSource.add(new NewsSource("The Next Web", getNewsid("The Next Web"), TECH, false));
        newsSource.add(new NewsSource("The Sport Bible", getNewsid("The Sport Bible"), SPORTS, false));
        newsSource.add(new NewsSource("The Telegraph", getNewsid("The Telegraph"), GENERAL, false));
        newsSource.add(new NewsSource("The Times of India", getNewsid("The Times of India"), GENERAL, false));
        newsSource.add(new NewsSource("The Verge", getNewsid("The Verge"), TECH, false));
        newsSource.add(new NewsSource("The Wall Street Journal", getNewsid("The Wall Street Journal"), BUS, false));
        newsSource.add(new NewsSource("The Washington Post", getNewsid("The Washington Post"), GENERAL, false));
        newsSource.add(new NewsSource("Time", getNewsid("Time"), GENERAL, false));
        newsSource.add(new NewsSource("USA Today", getNewsid("USA Today"), GENERAL, false));
        newsSource.add(new NewsSource("Wired.de", getNewsid("Wired de"), TECH, false));
        newsSource.add(new NewsSource("Wirtschafts Woche", getNewsid("Wirtschafts Woche"), BUS, false));


        return newsSource;
    }

    private String getNewsid(String newsName) {
        newsName = newsName.trim();
        String[] namesSplit = newsName.split(" ");
        StringBuilder nameid = new StringBuilder();
        for (String namej : namesSplit) {
            nameid.append(namej.toLowerCase());
            nameid.append("-");
        }
        String finalNameid = nameid.toString();
        finalNameid = finalNameid.substring(0, finalNameid.length() - 1);
        return finalNameid;
    }


    public void click(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
