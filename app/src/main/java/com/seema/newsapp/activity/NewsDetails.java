package com.seema.newsapp.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.seema.newsapp.R;
import com.seema.newsapp.interfaces.UpdateUi;
import com.seema.newsapp.module.Article;
import com.seema.newsapp.module.NewsResult;
import com.seema.newsapp.retrofitUtils.RetrofitNetworkCall;

import java.util.List;

public class NewsDetails extends AppCompatActivity implements UpdateUi {
    private static final String TAG = NewsDetails.class.getName();
    ArrayAdapter arrayAdapter;
    ListView listView;
    TextView mNoInternet;
    List<Article> articles;
    ProgressBar mProgressbar;
    String newsSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        String title = getIntent().getStringExtra("name");
        setTitle(title);

        newsSource = getIntent().getStringExtra("source");

        mNoInternet = findViewById(R.id.news_no_internet);
        mProgressbar = findViewById(R.id.news_progressbar);
        listView = findViewById(android.R.id.list);

        arrayAdapter = new ArrayAdapter();



        if (null != articles) {
            listView.setAdapter(arrayAdapter);
        }

        RetrofitNetworkCall.getNews(NewsDetails.this, newsSource);


       /* listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Uri uri = Uri.parse(articles.get(position).getUrl());

                CustomTabsIntent.Builder intent = new CustomTabsIntent.Builder();
                intent.setShowTitle(false);
                intent.setToolbarColor(getResources().getColor(R.color.colorPrimary));
                intent.build().launchUrl(NewsDetails.this, uri);

            }
        });*/


    }







    @Override
    public void updateNews(NewsResult newsResult) {

        mProgressbar.setVisibility(View.GONE);

        listView.setVisibility(View.VISIBLE);
        articles = newsResult.getArticles();
        listView.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();
    }


    @Override
    public void error(String error) {
        mProgressbar.setVisibility(View.GONE);
        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "error: " + error);
    }

    private class ArrayAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return articles.size();
        }

        @Override
        public Object getItem(int position) {
            return articles.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.news_details, parent, false);
            }
            TextView title = convertView.findViewById(R.id.news_title);
            title.setText(articles.get(position).getTitle().trim());

            ImageView imageView = convertView.findViewById(R.id.news_img);

            String url = articles.get(position).getUrlToImage();
            Glide.with(NewsDetails.this)
                    .load(url)
                    .into(imageView);

            TextView author = convertView.findViewById(R.id.news_author);
            String authorName = articles.get(position).getAuthor();
            if (null != authorName && !authorName.isEmpty()) {
                author.setText(authorName.trim());
            }

            TextView date = convertView.findViewById(R.id.news_date);
            String dateS = articles.get(position).getPublishedAtNewFormat();
            if (null != dateS && !dateS.isEmpty()) {
                date.setText(dateS/*.substring(0, 10).trim()*/);
            }


            ImageView mview  = convertView.findViewById(R.id.imageview);
            ImageView mshare = convertView.findViewById(R.id.imageshare);

            mview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri uri = Uri.parse(articles.get(position).getUrl());

                    CustomTabsIntent.Builder intent = new CustomTabsIntent.Builder();
                    intent.setShowTitle(false);
                    intent.setToolbarColor(getResources().getColor(R.color.colorPrimary));
                    intent.build().launchUrl(NewsDetails.this, uri);
                }
            });


            mshare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = articles.get(position).getDescription() + " " + articles.get(position).getUrl();
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, articles.get(position).getTitle());
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });

            return convertView;
        }
    }


}
