package com.seema.newsapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Bundle;
import android.widget.ListView;

import com.seema.newsapp.R;
import com.seema.newsapp.adapter.CategoryRecyclerViewAdapter;
import com.seema.newsapp.adapter.NewsSourceAdapter;
import com.seema.newsapp.database.DBHelper;
import com.seema.newsapp.module.NewsSource;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements CategoryRecyclerViewAdapter.CustomOnClickListener {

    RecyclerView recyclerView;
    ListView listView;
    ArrayList<NewsSource> sources;
    NewsSourceAdapter newsSourceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.newsCatRecyclerView);

        listView = findViewById(R.id.newsSourceListView);

        ArrayList<String> cats = new ArrayList<>();
        cats.add(SplashScreenActivity.FAVORITE);
        cats.add(SplashScreenActivity.GENERAL);
        cats.add(SplashScreenActivity.TECH);
        cats.add(SplashScreenActivity.ENT);
        cats.add(SplashScreenActivity.SPORTS);
        cats.add(SplashScreenActivity.SCI);
        cats.add(SplashScreenActivity.MUSIC);
        cats.add(SplashScreenActivity.GAME);
        cats.add(SplashScreenActivity.BUS);
        cats.add(SplashScreenActivity.POLITICAL);

        CategoryRecyclerViewAdapter adapter = new CategoryRecyclerViewAdapter(this, cats);
//        RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL);
//        GridLayoutManager manager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);

        DBHelper.init(this);
        sources = DBHelper.getNewsSources(SplashScreenActivity.GENERAL);
        newsSourceAdapter = new NewsSourceAdapter(this, sources);
        listView.setAdapter(newsSourceAdapter);
    }

    @Override
    public void onClick(String category) {
        DBHelper.init(this);
        if (category.equals(SplashScreenActivity.FAVORITE)) {
            sources = DBHelper.getNewsSourcesFav();
        } else {
            sources = DBHelper.getNewsSources(category);
        }

        newsSourceAdapter.notify(sources);
    }

}
